const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Container = db.container;
const Recipient = db.recipient;
const path12 = require("path");
const multer = require("multer");
const fs = require("fs");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const uploadFile = require("../middlewares/upload");
const uploadImg = require("../middlewares/uploadImg");
const uploadSingleImg = require("../middlewares/uploadSingleImg");
const { container } = require("../models");
// --------------------------To Create Users Based on roles-----------------------------------
exports.signup = (req, res) => {
  console.log(req.body.roles);
  // --------------------------Crypting the password-----------------------------------
  const password1 = bcrypt.hashSync(req.body.password);
  // Add data accordinhg to the model---------------------
  const user = new User({
    username: req.body.username,
    name: req.body.name,
    mobileNo: req.body.mobileNo,
    email: req.body.email,
    password: password1,
    // signature: "No signature",
    userStatus: "1",
    isSignin: false,
  });
  // --------------------------Mongo Query to Insert the Data-----------------------------------
  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    // --------------------------Query that Checks that the Role defined Exists in the Collection or not-----------------------------------
    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles } /*Check based on the name of the role*/,
        },
        (err, roles) => {
          /*If Error*/
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(
            (role) => role._id
          ); /*Otherwise it adds the id of that role*/
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "admin" }, (err, role) => {
        /*If no role defined then by default the user is of "Sender" type*/
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({
            message: "User was registered successfully!",
          }); /*Finally user is registered with roles*/
        });
      });
    }
  });
};

// --------------------------Update Profile----------------------------------------
exports.updateUserProfile = (req, res) => {
  // console.log(req.body);
  const username = req.body.username;
  const email = req.body.email;
  User.find({ username: username })
    .then((data) => {
      User.findByIdAndUpdate(
        data[0].id,
        { email: email },
        {
          useFindAndModify: false,
        }
      )
        .then((value) => res.status(200).send("Email updated!"))
        .catch((e) => console.log(e));
      // console.log(data);
    })
    .catch((e) => console.log(e));
};
// --------------------------To Add Containers-----------------------------------

exports.addContainer = (req, res) => {
  const email = req.body.email;
  const mobile = req.body.mobile;

  if (req.body.trackNo) {
    const trackNo = req.body.trackNo;
    Container.find({ trackNo: trackNo })
      .then((data) => {
        if (data.length == 0) {
          const recipientData = Recipient.findOne(
            /*Check to see if the Recipient Exists*/
            { email: email, mobile: mobile },
            (err, data) => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }
              // console.log(data);
              // ----------------------If No Recipient Exists--------------------------//
              if (data == null) {
                const recipientInfo = new Recipient({
                  email: email,
                  recipientName: req.body.recipientName,
                  mobile: mobile,
                });
                recipientInfo.save((err, value) => {
                  if (err) {
                    res.status(500).send({ message: err });
                    return;
                  }
                  if (req.body.handler) {
                    /*Check to find which type of user is handling the container*/
                    const handlerInfo = User.findOne(
                      { username: req.body.handler },
                      (err, handler) => {
                        if (err) {
                          res.status(500).send({
                            message:
                              "No Handler with username " +
                              req.body.handler +
                              "Found.",
                          });
                          return;
                        }

                        const containerInfo = new Container({
                          /*Finally Container Creation*/
                          trackNo: req.body.trackNo,
                          logisticsCompany: req.body.logisticsCompany,
                          transporterName: req.body.transporterName,
                          notes: req.body.notes,
                          signature: "No signature",
                          status: "In-Progress",
                          image1:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image1,
                          image2:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image2,
                          image3:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image3,
                        });

                        containerInfo.handler = handler._id;
                        containerInfo.recipient = value._id;

                        containerInfo.save((err) => {
                          if (err) {
                            res.status(500).send({ message: err });
                            return;
                          }
                          res.send({
                            message:
                              "Recipient and Container created successfully!",
                          });
                        });
                      }
                    );
                  }
                });
              } else {
                // ---------------------------------Recipient already exists--------------------------------//
                const recipientInfo = Recipient.findOne(
                  { email: email, mobile: mobile },
                  (err, data) => {
                    if (err) {
                      res.status(500).send({ message: err });
                      return;
                    }
                    // console.log("-----------", data);
                    const handlerInfo = User.findOne(
                      { username: req.body.handler },
                      (err, value) => {
                        if (err) {
                          res.status(500).send({ message: err });
                          return;
                        }
                        const containerInfo = new Container({
                          trackNo: req.body.trackNo,
                          logisticsCompany: req.body.logisticsCompany,
                          transporterName: req.body.transporterName,
                          notes: req.body.notes,
                          signature: "No signature",
                          status: "In-Progress",
                          image1:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image1,
                          image2:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image2,
                          image3:
                            process.cwd() +
                            "\\app\\containerImages\\" +
                            req.body.image3,
                        });

                        containerInfo.handler = value._id;
                        containerInfo.recipient = data._id;

                        containerInfo.save((err) => {
                          if (err) {
                            res.status(500).send({ message: err });
                            return;
                          }
                          res.send({
                            message:
                              "Container created for existing Recipient successfully!",
                          });
                        });
                      }
                    );
                  }
                );
              }
            }
          );
        } else {
          res.status(200).send({ message: "Container already exists!" });
        }
      })
      .catch((e) => console.log(e));
  } else {
    res.send({ message: "Track No not scanned properly. Scan Again!" });
  }
};
// --------------------------To Signin in the App-----------------------------------
exports.signin = (req, res) => {
  User.findOne({
    /*Check based on the username of the user*/
    username: req.body.username,
  })
    .populate(
      "roles",
      "-__v"
    ) /*To get the information of the Roles Collection */
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        /*Comparison to match the crypted passwords*/
        req.body.password,
        user.password
      );

      const statusObj = {
        /*Setting this parameter to True to reflect Succesful Login*/
        isSignin: true,
      };
      const id = user.id;
      User.findByIdAndUpdate(id, statusObj, {
        useFindAndModify: false,
      }) /*isSignin parameter updated of that particular user*/
        .then((data) => {
          console.log(data);
        })
        .catch((err) => {
          res.status(500).send({
            message: "Error ",
          });
        });

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        /*Creating jwt token*/
        expiresIn: 86400, // 24 hours
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        /*Getting Roles from the roles collection*/
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
        authorities.push(user.roles[i].id);
      }
      res.status(200).send({
        id: user._id,
        username: user.username,
        name: user.name,
        email: user.email,
        roles: authorities,
        // roles_id: roles.id,
        accessToken: token,
        status: user.status,
        signature: user.signature,
      });
    });
};
// --------------------------To Signout from the App-----------------------------------
exports.signout = (req, res) => {
  //-------------------------------- Signout Based on isSignin Flag-------------------------------
  User.findOne({
    username: req.body.username,
  })
    .then((data) => {
      const statusObj = {
        isSignin: false,
      };
      const id = data._id;
      User.findByIdAndUpdate(id, statusObj, { useFindAndModify: false })
        .then((data) => {
          res.status(200).send(data);
        })
        .catch((err) => {
          res.status(500).send({
            message: "Error ",
          });
        });
    })
    .catch((e) => console.log(e));
};

// --------------------------To Recover the Username through Email Service of Twilio-----------------------------------

exports.recoverUsername = (req, res) => {
  if (
    /*------------If Field are Empty------------------------*/
    req.body.nchoice == "username" &&
    req.body.password == "" &&
    req.body.email != ""
  ) {
    /*------------If Field are not Empty------------------------*/
    User.findOne({
      email: req.body.email,
    })
      .populate("roles", "-__v")
      .exec((err, user) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        if (!user) {
          return res.status(404).send({ message: "User Not found." });
        } else {
          /*------------If Query works Fine then send Email------------------------*/
          const sgmail = require("@sendgrid/mail");

          const API_KEY =
            "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";

          sgmail.setApiKey(API_KEY);

          const msg = {
            to: user.email,
            from: {
              name: "Podpic",
              email:
                "prerna.sinha1809@gmail.com" /*------------email of a verified user on Twilio------------------------*/,
            },
            subject: "Podpic Recover Username",
            text: `The registered username for this Email: ${user.username}`,
            html: `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
            <head>
            <!--[if gte mso 9]>
            <xml>
              <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
              </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <meta name="x-apple-disable-message-reformatting">
              <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
              <title></title>
              
                <style type="text/css">
                  table, td { color: #000000; } @media only screen and (min-width: 520px) {
              .u-row {
                width: 500px !important;
              }
              .u-row .u-col {
                vertical-align: top;
              }
            
              .u-row .u-col-100 {
                width: 500px !important;
              }
            
            }
            
            @media (max-width: 520px) {
              .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
              }
              .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
              }
              .u-row {
                width: calc(100% - 40px) !important;
              }
              .u-col {
                width: 100% !important;
              }
              .u-col > div {
                margin: 0 auto;
              }
            }
            body {
              margin: 0;
              padding: 0;
            }
            
            table,
            tr,
            td {
              vertical-align: top;
              border-collapse: collapse;
            }
            
            p {
              margin: 0;
            }
            
            .ie-container table,
            .mso-container table {
              table-layout: fixed;
            }
            
            * {
              line-height: inherit;
            }
            
            a[x-apple-data-detectors='true'] {
              color: inherit !important;
              text-decoration: none !important;
            }
            
            </style>
              
              
            
            </head>
            
            <body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
              <!--[if IE]><div class="ie-container"><![endif]-->
              <!--[if mso]><div class="mso-container"><![endif]-->
              <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%" cellpadding="0" cellspacing="0">
              <tbody>
              <tr style="vertical-align: top">
                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #e7e7e7;"><![endif]-->
                
            
            <div class="u-row-container" style="padding: 0px;background-color: transparent">
              <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 500px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px;"><tr style="background-color: transparent;"><![endif]-->
                  
            <!--[if (mso)|(IE)]><td align="center" width="500" style="width: 500px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
            <div class="u-col u-col-100" style="max-width: 320px;min-width: 500px;display: table-cell;vertical-align: top;">
              <div style="width: 100% !important;">
              <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
              
            <table style="font-family:arial,helvetica,sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;" align="left">
                    
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td style="padding-right: 0px;padding-left: 0px;" align="center">
                  
                  <img align="center" border="0" src="images/image-1.png" alt="Podpic logo" title="Podpic logo" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 20%;max-width: 96px;" width="96"/>
                  
                </td>
              </tr>
            </table>
            
                  </td>
                </tr>
              </tbody>
            </table>
            
            <table style="font-family:arial,helvetica,sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;" align="left">
                    
              <div style="color: #ce4444; line-height: 140%; text-align: left; word-wrap: break-word;">
                <p style="font-size: 14px; line-height: 140%; text-align: center;"><span style="font-family: 'arial black', 'avant garde', arial; font-size: 22px; line-height: 30.8px;">Welcome to Podpic</span></p>
              </div>
            
                  </td>
                </tr>
              </tbody>
            </table>
            
            <table style="font-family:arial,helvetica,sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;" align="left">
                    
              <div style="line-height: 140%; text-align: left; word-wrap: break-word;">
                <p style="font-size: 14px; line-height: 140%; text-align: center;"><span style="font-size: 16px; line-height: 22.4px;"><strong>Username Recovery Email</strong></span></p>
            <p style="font-size: 14px; line-height: 140%; text-align: center;">&nbsp;</p>
            <p style="font-size: 14px; line-height: 140%; text-align: center;">@${user.email}</p>
              </div>
            
                  </td>
                </tr>
              </tbody>
            </table>
            
            <table style="font-family:arial,helvetica,sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;" align="left">
                    
              <table height="0px" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #BBBBBB;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                <tbody>
                  <tr style="vertical-align: top">
                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                      <span>&#160;</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            
                  </td>
                </tr>
              </tbody>
            </table>
            
            <table style="font-family:arial,helvetica,sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;" align="left">
                    
              <div style="line-height: 140%; text-align: left; word-wrap: break-word;">
                <p style="font-size: 14px; line-height: 140%; text-align: center;">The username for this email id is <strong>${user.username}.</strong></p>
            <p style="font-size: 14px; line-height: 140%; text-align: center;"><strong><br /></strong>Kindly use this username for a successful login.</p>
              </div>
            
                  </td>
                </tr>
              </tbody>
            </table>
            
              <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
              </div>
            </div>
            <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
            
            
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </td>
              </tr>
              </tbody>
              </table>
              <!--[if mso]></div><![endif]-->
              <!--[if IE]></div><![endif]-->
            </body>
            
            </html>
            `,
          };

          sgmail
            .send(msg)
            .then((response) => {
              console.log("Message Sent");
            })
            .catch((error) => {
              res.send("------------------------------", error);
              // console.log(error.response.body.errors[0].message)
            });
        }
      });
  } else if (
    req.body.nchoice == "password" &&
    req.body.password != "" &&
    req.body.email != ""
  ) {
    /*------------If Password needs to updated------------------------*/
    User.findOne({
      email: req.body.email,
    })
      .populate("roles", "-__v")
      .exec((err, user) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        if (!user) {
          return res.status(404).send({ message: "User Not found." });
        } else {
          const password1 = bcrypt.hashSync(
            req.body.password
          ); /*------------Password Cryption------------------------*/
          User.findOneAndUpdate(
            /*------------Update password query------------------------*/
            { email: req.body.email },
            { password: password1 },
            { useFindAndModify: false },
            (err, data) => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              } else {
                console.log("required", data.password);
                res.send("Password Updated!");
              }
            }
          );
        }
      });
  } else {
    console.log("Something Went wrong!");
  }
};

// --------------------------To Upload File-----------------------------------

exports.upload = async (req, res) => {
  try {
    await uploadFile(req, res);
    console.log("------", req.file);
    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }
    res.status(200).send({
      message: "Uploaded the file successfully: " + req.file.originalname,
    });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  }
};

exports.sendDocEmail = (req, res) => {
  const sgmail = require("@sendgrid/mail");

  const API_KEY =
    "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";

  sgmail.setApiKey(API_KEY);

  const fs = require("fs");
  if (!req.body.flag) {
    const pathToAttachment1 =
      process.cwd() + "\\containerImages\\" + req.body.imgArr[0];
    const attachment1 = fs.readFileSync(pathToAttachment1).toString("base64");
    if (req.body.imgArr.length <= 1) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>" + req.body.text + "</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    } else if (req.body.imgArr.length < 3) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>" + req.body.text + "</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[1],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    } else if (req.body.imgArr.length < 4) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>" + req.body.text + "</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[1],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[2],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    }
  } else if (req.body.flag) {
    const pathToAttachment1 =
      process.cwd() + "\\containerImages\\" + req.body.imgArr[0];
    const attachment1 = fs.readFileSync(pathToAttachment1).toString("base64");
    if (req.body.imgArr.length <= 1) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>This is final transporter email</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    } else if (req.body.imgArr.length < 3) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>This is final transporter email</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[1],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    } else if (req.body.imgArr.length < 4) {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>This is final transporter email</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[1],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[2],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    } else {
      const msg = {
        to: req.body.email,
        from: {
          name: "Podpic",
          email: "prerna.sinha1809@gmail.com",
        },
        subject: "Podpic Package Documents",
        text: "its great to send emails from Sendgrid",
        html: "<h1>This is final transporter email</h1>",

        attachments: [
          {
            content: attachment1,
            filename: req.body.imgArr[0],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[1],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[2],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
          {
            content: attachment1,
            filename: req.body.imgArr[3],
            type: "PodPic/jpeg",
            disposition: "attachment",
          },
        ],
      };

      sgmail
        .send(msg)
        .then((response) => {
          res.send("Message sent");
        })
        .catch((error) => {
          console.log(error);
          // console.log(error.response.body.errors[0].message)
        });
    }
  }
};

// --------------------------To Upload Multiple Images-----------------------------------
exports.uploadMultiImage = async (req, res) => {
  try {
    await uploadImg(req, res);
    const PDFDocument = require("pdfkit");
    const fs = require("fs");

    const doc = new PDFDocument();

    doc.pipe(
      fs.createWriteStream(process.cwd() + "//app//controllers//container.pdf")
    );

    doc.fontSize(15).text("Picture of the Container", 100, 100);
    doc.image(process.cwd() + "//containerImages//" + req.file.originalname, {
      fit: [500, 500],
      align: "centre",
      valign: "center",
    });

    doc.end();
    // console.log("this is req.file", req.file);
    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }
    res.status(200).send({
      message: req.file.originalname,
    });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  }
};

// --------------------------To upload Single Image-----------------------------------

exports.uploadSingleImage = async (req, res) => {
  try {
    await uploadSingleImg(req, res);
    // const sharp = require("sharp");
    const tesseract = require("tesseract.js");
    const PDFDocument = require("pdfkit");
    const fs = require("fs");

    const doc = new PDFDocument();

    doc.pipe(
      fs.createWriteStream(process.cwd() + "//app//controllers//example-1.pdf")
    );
    console.log("------", req.file);

    // sharp("./" + req.file.path)
    //   .resize(500, 500, {
    //     fit: "contain",
    //   })

    //   .toFile("./images/src/out_" + req.file.originalname, (err, info) => {
    //     console.log(err);
    //   });

    tesseract
      .recognize(process.cwd() + "//images//" + req.file.originalname, "eng", {
        logger: (e) => console.log(e),
      })
      .then(({ data: { text } }) => {
        console.log(text);
        res.status(200).send({
          message: text,
        });
        // res.send(text)
        doc.fontSize(15).text(text, 100, 100);

        doc.end();
      });

    // console.log("-----------", info);
    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }
    // res.status(200).send({
    //   message: "Uploaded the file successfully: " + req.file.originalname,
    // });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  } // uploadImg(req, res);
};

// --------------------------To Add Digital signatures-----------------------------------

exports.addDigitalSign = (req, res) => {
  Container.find({ trackNo: req.body.trackNo })
    .then((data) => {
      // res.send(data);
      const id = data[0]._id;
      const info = {
        signature: req.body.signature,
        status: req.body.status,
      };
      Container.findByIdAndUpdate(id, info, { useFindAndModify: false })
        .then((data) => {
          if (!data) {
            res.send({
              message: `Cannot update Signature. Maybe Signature was not found!`,
            });
          } else res.send({ message: "Signature was updated successfully." });
        })
        .catch((err) => {
          res.status(500).send({
            message: "Error updating Signature ",
          });
        });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving user Info.",
      });
    });
  // }
};

// --------------------------To Upload Multiple Images-----------------------------------

exports.upload4Image = async (req, res) => {
  try {
    await uploadImg(req, res);
    console.log(req.file);
    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }
    res.status(200).send({
      message: req.file.originalname,
    });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  } // uploadImg(req, res);
};

// --------------------------To Get All Containers-----------------------------------

exports.getAllContainers = (req, res) => {
  User.find({ username: req.body.username })
    .then((data) => {
      Container.find({ handler: data[0]._id })
        .then((value) => {
          res.send(value);
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message ||
              "Couldn't find Container with handler " + data.username,
          });
        });
    })
    .catch((err) => {
      res.status(500).send({
        message: "A User of username " + req.body.username + " doesn't exist.",
      });
    });
};

exports.getAllRecipient = (req, res) => {
  Container.find({ trackNo: req.body.trackNo })
    .then((value) => {
      Recipient.find({ _id: value[0].recipient[0] })
        .then((data) => res.send(data[0].email))
        .catch((err) => {
          res.status(500).send({
            message: err.message || "Couldn't find Recipient with trackNo",
          });
        });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Couldn't find Container with trackNo",
      });
    });
};
// -------------------------------------Admin Apis----------------------------------------------//

// ----------------------------------Get All Users----------------------------------------------//
exports.getAllUsers = (req, res) => {
  User.findOne({ username: req.body.username })
    .then((data) => {
      User.find({ username: { $ne: "Admin" } })
        .then((data) => res.send(data))
        .catch((err) => console.log(err));
    })
    .catch((err) => {
      res.status(500).send({
        message: err,
      });
    });
};

// ---------------------------------------Get All Container Records---------------------

exports.getAllContainersAdmin = (req, res) => {
  User.findOne({
    username: req.body.username,
  }) /* ----Query to validate Admin and get all containers----------- */
    .then((data) => {
      Container.find({ handler: { $ne: [data._id] } })
        .then((data) => res.send(data))
        .catch((err) => console.log(err));
    })
    .catch((err) => {
      res.status(500).send({
        message: err,
      });
    });
};

// ---------------------------------Update User Credentials----------------------------------

exports.updateUserStatus = (req, res) => {
  User.findOne({
    username: req.body.username,
  })
    .then((data) => {
      User.find({ email: req.body.email })
        .then((data) => {
          const id = data[0]._id;
          if (data[0].userStatus == "0") {
            /*--------------If User Status Inactive-------------------*/
            const changeStatus = {
              userStatus: "1",
            };
            User.findByIdAndUpdate(id, changeStatus, {
              useFindAndModify: false,
            })
              .then((data) => {
                res.send(data);
              })
              .catch((err) => {
                res.status(500).send({
                  message: "Error",
                });
              });
          } else {
            const changeStatus = {
              /*--------------If User Status active-------------------*/
              userStatus: "0",
            };
            User.findByIdAndUpdate(id, changeStatus, {
              useFindAndModify: false,
            })
              .then((data) => {
                res.send(data);
              })
              .catch((err) => {
                res.status(500).send({
                  message: "Error",
                });
              });
          }
        })
        .catch((err) => res.send(err));
    })
    .catch((err) => {
      res.status(500).send({
        message: err,
      });
    });
};
