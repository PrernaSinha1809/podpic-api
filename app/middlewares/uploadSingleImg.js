const util = require("util");
const multer = require("multer");

const StorageImg = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./images");
  },
  filename(req, file, cb) {
    console.log(file);
    cb(null, file.originalname);
  },
});

let uploadImg = multer({
  storage: StorageImg,
}).single("photo");

let uploadImgMiddleware = util.promisify(uploadImg);
module.exports = uploadImgMiddleware;
