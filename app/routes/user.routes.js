const { authJwt } = require("../middlewares");
const controller = require("../controllers/sender.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/admin", controller.allAccess);

  app.get("/api/test/sender", [authJwt.verifyToken], controller.senderBoard);

  app.get(
    "/api/test/transporter",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.transporterrBoard
  );

  app.get(
    "/api/test/courier",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.courierBoard
  );
};
