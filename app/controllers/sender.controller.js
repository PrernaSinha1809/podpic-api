exports.allAccess = (req, res) => {
  res.status(200).send('Public Content.');
};

exports.senderBoard = (req, res) => {
  res.status(200).send('Sender Content.');
};

exports.courierBoard = (req, res) => {
  res.status(200).send('Courier Content.');
};

exports.transporterrBoard = (req, res) => {
  res.status(200).send('Transporter/Shipper Content.');
};
