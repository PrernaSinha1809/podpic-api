const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUsernameOrEmail,
      verifySignUp.checkRolesExisted,
    ],
    controller.signup
  );

  app.post("/api/auth/signin", controller.signin);
  app.post("/api/auth/updateProfile", controller.updateUserProfile);
  app.post("/api/auth/rcrUsername", controller.recoverUsername);
  app.post("/api/auth/sendEmail", controller.sendDocEmail);
  // app.post("/api/auth/jpgToPdf", controller.pdfToJson);
  app.post("/api/auth/upload", controller.upload);
  // app.post("/api/auth/ocr", controller.ocrapi);
  app.post("/api/auth/uploadImg", controller.uploadMultiImage);
  app.post("/api/auth/uploadSingleImg", controller.uploadSingleImage);
  app.post("/api/auth/addSign", controller.addDigitalSign);
  app.post("/api/auth/uploadFinalContainer", controller.upload4Image);
  app.post("/api/auth/signout", controller.signout);
  app.post("/api/auth/addContainer", controller.addContainer);
  app.post("/api/auth/getAllContainers", controller.getAllContainers);
  app.post("/api/auth/getRecipient", controller.getAllRecipient);
  /*----------------Admin Urls--------------------*/
  app.post("/api/auth/getAllUsersAdmin", controller.getAllUsers);
  app.post(
    "/api/auth/getAllContainerssAdmin",
    controller.getAllContainersAdmin
  );
  app.post("/api/auth/updateUserStatusAdmin", controller.updateUserStatus);
};
