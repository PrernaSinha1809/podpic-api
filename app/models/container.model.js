const mongoose = require("mongoose");

const Container = mongoose.model(
  "Container",
  new mongoose.Schema({
    trackNo: String,
    logisticsCompany: String,
    transporterName: String,
    notes: String,
    image1: String,
    image2: String,
    image3: String,
    signature: String,
    status: String,
    recipient: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recipient",
      },
    ],
    handler: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Sender",
      },
    ],
  })
);

module.exports = Container;
