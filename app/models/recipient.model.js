const mongoose = require("mongoose");

const Container = mongoose.model(
  "Recipient",
  new mongoose.Schema({
    recipientName: String,
    email: String,
    mobile: String,
  })
);

module.exports = Container;
