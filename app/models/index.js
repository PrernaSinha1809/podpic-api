const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./sender.model");
db.role = require("./role.model");
db.container = require("./container.model");
db.recipient = require("./recipient.model");

db.ROLES = ["sender", "transporter/shipper", "courier", "admin"];

module.exports = db;
